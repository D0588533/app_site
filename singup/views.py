from singup.models import product
from django.shortcuts import render
from django.http import HttpResponse, Http404
import random

# Create your views here.

# def about(request):
#     html = '''
# <!DOCTYPE html>
# <html><head><title>About Myself</title></head>
# <body><h2>Min-Huang Ho</h2><hr>
# <p>Hi, I am Min-Huang Ho. Nice to meet you!</p>
# </body>
# </html>
#     '''
#     return HttpResponse(html)

def listing(request):
    dom="""
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset='utf-8'>
    <title>中古機列表</title>
    </head>
    <body>
    <h2>以下是目前本店販售中的二手機列表</h2>
    <hr>
    <table width=400 border=1 bgcolor='#ccffcc'>
    {}
    </table>
    </body>
    </html>
    """
    ####################################################

    products  = product.objects.all()
    tags = '<tr><td>品名</td><td>售價</td><td>庫存量</td></tr>'
    for p in products:
        tags = tags + '<tr>'
        tags = tags + '<td>{}</td>'.format(p.name)
        tags = tags + '<td>{}</td>'.format(p.price)
        tags = tags + '<td>{}</td>'.format(p.qty)
        tags = tags + '</tr>'

    ####################################################

    return HttpResponse(dom.format(tags))

def disp_detail(request,id):
    # dom = """
    # <!DOCTYPE html>
    # <html>
    # <head>
    # <meta charset='utf-8'>
    # <title>{}</title>
    # </head>
    # <body>
    # <h2>{}</h2>
    # <hr>
    # <table width=400 border=1 bgcolor='#ccffcc'>
    # {}
    # </table>
    # <a href='/list'>回列表</a>
    # </body>
    # </html>    
    # """
    ####################################################
    try:
        p = product.objects.get(id=id)
    except product.DoesNotExist:
        raise Http404('找不到指定的品項')
        
    # tags = '<tr><td>品項編號</td><td>{}</td></tr>'.format(p.id)
    # tags = tags + '<tr><td>品項名稱</td><td>{}</td></tr>'.format(p.name)
    # tags = tags + '<tr><td>建議售價</td><td>{}</td></tr>'.format(p.price)
    # tags = tags + '<tr><td>庫存數量</td><td>{}</td></tr>'.format(p.qty)

    ####################################################
    # return HttpResponse(dom.format(p.name, p.name, tags))
    return render(request, "disp.html", locals())
        
### import random
def about(request):
    quotes = [  '今日事，今日畢',
                '要怎麼收穫，先那麼栽',
                '知識就是力量',
                '一個人的個性就是他的命運']
    # rand = 0
    # quote = '今日事，今日畢'
    quote = random.choice(quotes)
    return render(request, 'about.html', locals())